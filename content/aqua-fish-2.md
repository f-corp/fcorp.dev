---
title: "Aqua Fish 2"
slug: "aqua-fish-2"
date: 2024-03-01
description: "You can't stop the evoulution"
kind: page
image: "images/aquafish2.jpg"
web: "https://aquafish2.fcorp.dev/"
next: birth-of-redemption
---

Straight from the mind of our physics expert John: Aqua Fish is back! The sequel to the critically acclaimed nextgen oceanic wildlife simulator Aqua Fish. Featuring more water, more fish, huge performance improvements and an in-depth revolutionary evolutionary systems. Prepare your mind for submersion and immersion.
