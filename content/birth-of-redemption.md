---
title: "Birth of Redemption Prototype"
slug: "birth-of-redemption-prototype"
date: 2021-03-26T00:43:32+01:00
description: "Experience the next gen FPS from Furniture Corporation"
kind: page
image: "images/birth-of-redemption.jpg"
next: aqua-fish
---

This FPS prototype offers a look a the possibilities of the Godot (*god-oh*) engine and how things can (but probably shouldn't) be done. Mantle the heavy burden of $HERO_NAME and confront the evil antagonists on $SPACESHIP_NAME. Featuring realistic physics, multiple weapons and a whole lot of custom assets.
