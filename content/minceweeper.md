---
title: "Minceweeper"
slug: "minceweeper"
date: 2024-03-02
description: "Weep like you never wept before"
kind: page
image: "images/minceweeper.jpg"
linux: "https://gitlab.com/f-corp/minceweeper/uploads/b32c2111a034564d72840f8a2efc969e/minceweeper-1.0.0_linux.x86-64"
next: aqua-fish-2
---

"*Look at you, hacker: a minceweeper of meat and bone.*" Your path to victory? You have to clear the mines. The numbers, are they trying to tell you something? Scroll back in time and retrace your steps. Compete with your friends on any of the 4096 seeds and an unlimited (max 40x40) board size, filled to the brim with danger. Will you prevail, or will you Die By The Mine?
