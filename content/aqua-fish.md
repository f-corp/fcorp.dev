---
title: "Aqua Fish"
slug: "aqua-fish"
date: 2021-03-26T00:43:32+01:00
description: "Experience the next-gen oceanic wildlife simulator"
kind: page
image: "images/aquafish.jpg"
windows: "https://github.com/fuggla/fc-aqua-fish/releases/download/v1.0/aqua-fish-1.0.zip"
---

You can now enjoy a live simulation of how fish interact with each other. Make godlike decisions over a specicies future by controlling the biomes population, food supply and hunting mechanisms.  Features realistic physics and specicies specific artificial intelligence. Our first game, and a huge international success on a very local scale.
